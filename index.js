
fetch("https://jsonplaceholder.typicode.com/todos")
{
	id: false,
	title: true,
	userId:false
}
.th
.then(response => response.json())en(json => console.log(json));

/*Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json));

/*Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.*/


async function fetchData() {
	let result = await fetch("https://jsonplaceholder.typicode.com/todos/1");
	
	console.log(result);

	console.log(typeof result);
	
	console.log(result.body);

	let json = await result.json();
	
	console.log(json);
};

fetchData();


/*Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API*/

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	
	body: JSON.stringify({
		title: "New created to do List",
		body: "New  created to do list",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));


/* Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API*/
/*Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID*/

	fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PUT",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			id: 1,
			title:"Updated Post",
			description: "New updated Post",
			status: "completed",
			datecompleted: "July 22, 2022",
			userId: 1
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));

/*Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API*/

	fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			title:"Updated and Corrected Post"
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));


/*Update a to do list item by changing the status to complete and add a date when the status was changed*/

	fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			title:"Updated and Corrected Post",
			status: "Completed",
			date: "July 22, 2022"
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));


/*Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.*/

	fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE"
	} )
// Create a request via Postman to retrieve all the to do list items.

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "GET",
	headers: {
		"Content-Type": "application/json"
	}
})
.then(response => response.json())
.then(json => console.log(json));

//  Create a request via Postman to retrieve an individual to do list item
// https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// Save this request as get to do list item

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "GET",
	headers: {
		"Content-Type": "application/json"
	}
})
.then(response => response.json())
.then(json => console.log(json));

/*15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpointcd d
- Save this request as create to do list item*/

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	
	body: JSON.stringify({
		title: "Create to do list item",
		body: "Create to do list item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

/*16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request*/

fetch( "https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		id: 1,
		title:"Updated to do list item PUT",
		description: "Updated Post to do list item PUT",
		userId: 1
	})
} )
.then(response => response.json())
.then(json => console.log(json));


/*17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PATCH
- Update the to do list item to mirror the data structure of the PATCH fetch request*/

	fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
		id: 1,
		title:"Updated to do list item PATCH",
		description: "Updated Post to do list item PATCH"

		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));
/*18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item*/

fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE"
	} )

